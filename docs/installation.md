# Installation

## Requirements

- QGIS 3.16+
- some Python packages

Typically on Ubuntu:

```bash
python3 -m pip install -U -r requirements/base.txt
```

<!-- ## Stable version (recomended)

This plugin is published on the official QGIS plugins repository: <https://plugins.qgis.org/plugins/french_locator_filter/>.

## Beta versions released

Enable experimental extensions in the QGIS plugins manager settings panel. -->

## Earlier development version

If you define yourself as early adopter or a tester and can't wait for the release, the plugin is automatically packaged for each commit to master, so you can use this address as repository URL in your QGIS extensions manager settings:

```url
https://oslandia.gitlab.io/qgis/QSoccer/plugins.xml
```

Be careful, this version can be unstable.
