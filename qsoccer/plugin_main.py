#! python3  # noqa: E265

"""
    Main plugin module.
"""


# PyQGIS
from qgis.core import QgsApplication, QgsProject
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication, Qt
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QDockWidget
from qgis.utils import showPluginHelp

# project
from qsoccer.__about__ import DIR_PLUGIN_ROOT, QGS_PROJECT_DIR, __title__, __version__
from qsoccer.gui.dlg_settings import PlgOptionsFactory
from qsoccer.gui.qsoccer_dialog import QSoccerDialog
from qsoccer.statsbomb import StatsbombDataset
from qsoccer.toolbelt import PlgLogger, PlgTranslator

# ############################################################################
# ########## Classes ###############
# ##################################


class QsoccerPlugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.log = PlgLogger().log

        # translation
        plg_translation_mngr = PlgTranslator()
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr

        self.__config = None
        self.__layer = None
        self.__project = None
        # Root widget, to attach children to
        self.__docks = []
        self.view_pitch = None

        # The Statsbomb dataset is instanciated with the plugin
        # and the competition data is loaded in order to get the competition IDs.
        self.__dataset = StatsbombDataset()
        self.__dataset.load_competitions()

    def initGui(self):
        """Set up plugin UI elements."""

        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        # -- Actions
        self.action_help = QAction(
            QIcon(":/images/themes/default/mActionHelpContents.svg"),
            self.tr("Help", context="QsoccerPlugin"),
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            lambda: showPluginHelp(filename="resources/help/index")
        )

        self.action_settings = QAction(
            QgsApplication.getThemeIcon("console/iconSettingsConsole.svg"),
            self.tr("Settings"),
            self.iface.mainWindow(),
        )
        self.action_settings.triggered.connect(
            lambda: self.iface.showOptionsDialog(
                currentPage="mOptionsPage{}".format(__title__)
            )
        )

        # -- Menu
        self.iface.addPluginToMenu(__title__, self.action_settings)
        self.iface.addPluginToMenu(__title__, self.action_help)

        # -- Init the pitch widget
        pitch_icon = QIcon(
            str(DIR_PLUGIN_ROOT / "resources/images/qsoccer_logo_48px.svg")
        )
        self.view_pitch = QAction(pitch_icon, "View pitch", self.iface.mainWindow())
        self.view_pitch.triggered.connect(lambda: self.on_view_pitch())
        self.iface.addToolBarIcon(self.view_pitch)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up menu
        self.iface.removePluginMenu(__title__, self.action_help)
        self.iface.removePluginMenu(__title__, self.action_settings)

        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)

        # remove actions
        del self.action_settings
        del self.action_help

        # remove widget docks
        self.__docks = []
        self.iface.removeToolBarIcon(self.view_pitch)
        del self.view_pitch

    def run(self):
        """Main process.

        :raises Exception: if there is no item in the feed
        """
        try:
            self.log(
                message=self.tr(
                    text="Everything ran OK.",
                    context="QsoccerPlugin",
                ),
                log_level=3,
                push=False,
            )
        except Exception as err:
            self.log(
                message=self.tr(
                    text="Houston, we've got a problem: {}".format(err),
                    context="QsoccerPlugin",
                ),
                log_level=2,
                push=True,
            )

    def on_view_pitch(self):
        """Open main QSoccer dock"""

        dock = QDockWidget(f"{__title__} - {__version__}")
        dock.setObjectName("QSoccer_dock")
        dialog = QSoccerDialog(dock, self.iface, self.__dataset)
        dock.setWidget(dialog)
        if len(self.__docks) == 0:
            self.iface.addDockWidget(Qt.LeftDockWidgetArea, dock)
        else:
            self.iface.mainWindow().tabifyDockWidget(self.__docks[-1], dock)
        self.__docks.append(dock)

        # Read demo project
        self.__project = QgsProject.instance()
        project_name = QGS_PROJECT_DIR.resolve().joinpath("demo.qgs")
        self.__project.read(str(project_name))