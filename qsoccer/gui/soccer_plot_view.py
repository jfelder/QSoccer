#
#   Copyright (C) 2018 Oslandia <infos@oslandia.com>
#
#   This file is a piece of free software; you can redistribute it and/or
#   modify it under the terms of the GNU Library General Public
#   License as published by the Free Software Foundation; either
#   version 2 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Library General Public License for more details.
#   You should have received a copy of the GNU Library General Public
#   License along with this library; if not, see <http://www.gnu.org/licenses/>.
#

"""
Set of classes used to draw both "vertical" and "horizontal" plots.

The "X" axis in the code below refers to the dimension against which
the "Y" dimension is observed: e.g. time for timeseries or depth for well logs.

The X axis may also be named "domain axis".
The Y axis may also be named "value axis".
"""
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from mplsoccer.pitch import Pitch
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QGraphicsScene, QGraphicsView, QVBoxLayout, QWidget


class SoccerPitchPlotGraphicsView(QGraphicsView):
    def __init__(self, scene, parent=None):
        """View handles user interaction, like resize, mouse etc

        :param scene: MyScene, heritated from QGraphicScene
        :type scene: MyScene
        :param orientation: orientation of the plot windows
        :type orientation: bool
        :param parent: parent
        :type parent: QObjet
        """
        QGraphicsView.__init__(self, scene, parent)

        self.pitch = Pitch(pitch_color="grass", line_color="white", stripe=True)

        fig = Figure()
        self.canvas = FigureCanvas(fig)
        self.canvas.figure, self.ax = self.pitch.draw()
        self.canvas.setGeometry(0, 0, 600, 400)
        scene.addWidget(self.canvas)

        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    def reset(self):
        self.ax.clear()
        self.pitch.draw(ax=self.ax)
        self.canvas.draw()

    def plot_heatmap(self, events):
        self.reset()
        self.pitch.kdeplot(
            events["coordinates_x"],
            events["coordinates_y"],
            ax=self.ax,
            shade=True,
            cmap="rocket",
            n_levels=80,
            alpha=0.67,
            antialiased=True,
        )
        self.canvas.draw()

    def plot_passes(self, events):
        self.reset()
        self.pitch.arrows(
            events["coordinates_x"],
            events["coordinates_y"],
            events["end_coordinates_x"],
            events["end_coordinates_y"],
            width=2,
            headwidth=10,
            headlength=10,
            color="#ad993c",
            ax=self.ax,
            label="completed passes",
        )
        self.canvas.draw()

    def plot_shots(self, events):
        self.reset()
        self.pitch.arrows(
            events["coordinates_x"],
            events["coordinates_y"],
            events["end_coordinates_x"],
            events["end_coordinates_y"],
            width=2,
            headwidth=10,
            headlength=10,
            color="#004bbd",
            ax=self.ax,
            label="shots",
        )
        self.canvas.draw()


class SoccerPitchWidget(QWidget):
    """Plot view widget"""

    def __init__(self, parent=None):
        """Plot Widget inserted into main dialog.
        :param parent: parent
        :type parent: QObject

        """
        super().__init__(parent)

        self._scene = QGraphicsScene(0, 0, 600, 400)
        self.__view = SoccerPitchPlotGraphicsView(self._scene)

        box = QVBoxLayout()
        box.addWidget(self.__view)
        self.setLayout(box)

    def plot(self, plot_type, events):
        if plot_type == "passes":
            self.__view.plot_passes(events)
        elif plot_type == "shots":
            self.__view.plot_shots(events)
        elif plot_type == "heatmap":
            self.__view.plot_heatmap(events)
        else:
            self.log(message="Bad plot type", log_level=1)
        self._scene.update()
