# QSoccer

[![pipeline status](https://gitlab.com/Oslandia/qgis/QSoccer/badges/master/pipeline.svg)](https://gitlab.com/Oslandia/qgis/QSoccer/-/commits/master)
[![documentation badge](https://img.shields.io/badge/documentation-autobuilt%20with%20Sphinx-blue)](https://oslandia.gitlab.io/qgis/QSoccer/)

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

![QSoccer_logo](./qsoccer/resources/images/qsoccer_logo_512px.svg)

Display some football data onto a QGIS plugin.

## Documentation

[:book: Check-out the documentation](https://oslandia.gitlab.io/qgis/QSoccer/)

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md)

## License

`QSoccer` is free software and licenced under the GNU GPL version 2 or any later version. See [LICENSE](./LICENSE) file.
